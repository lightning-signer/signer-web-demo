#!/bin/env python3

import logging
import asyncio
from threading import Condition, Lock

from flask import Flask, render_template
from flask_socketio import SocketIO
import grpc

from service_pb2 import PingReply, InitReply, CallReply, NewClientReply
from service_pb2_grpc import DemoServicer, add_DemoServicer_to_server

logger: logging.Logger
app = Flask(__name__)

app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)
outstanding = {}


@socketio.on('connect')
def handle_connect():
    print("socket connected")


@socketio.on('response')
def handle_response(data):
    entry = outstanding[data['id']]
    entry[1] = data
    with entry[0]:
        entry[0].notify()


@app.route('/')
def index():
    return render_template('index.html')


class Servicer(DemoServicer):
    def __init__(self):
        super(Servicer, self).__init__()
        self.counter = 0
        self.lock = Lock()
        self.instance_id = 0

    def next_req_id(self):
        with self.lock:
            self.counter += 1
            req_id = self.counter
        return req_id

    async def Ping(self, request, context):
        logger.info("ping")
        return PingReply(body=request.body + " reply")

    async def Init(self, request, context):
        with self.lock:
            self.instance_id += 1
            instance_id = self.instance_id
        logger.info(f"init instance {instance_id}")
        _res = await self.call_on_socketio(
            "init",
            {
                'instance_id': instance_id,
                'seed': request.seed,
            })
        logger.info(f"done init instance {instance_id}")
        return InitReply(instance_id=instance_id)

    async def NewClient(self, request, context):
        client_id = request.client_id
        instance_id = request.instance_id
        logger.info(f"new client {client_id}/{instance_id}")

        _res = await self.call_on_socketio(
            "new_client",
            {
                'instance_id': instance_id,
                'client_id': client_id,
                'dbid': request.dbid,
                'peer_id': request.peer_id,
            })
        return NewClientReply()

    async def Call(self, request, context):
        client_id = request.client_id
        instance_id = request.instance_id
        logger.info(f"client call {client_id}/{instance_id}")

        if client_id == 0:
            result = await self.call_on_socketio(
                "root_request",
                {
                    'instance_id': instance_id,
                    'body': request.body
                })
        else:
            result = await self.call_on_socketio(
                "client_request",
                {
                    'instance_id': instance_id,
                    'client_id': client_id,
                    'body': request.body
                })
        return CallReply(body=result['body'])

    async def call_on_socketio(self, name, args):
        req_id = self.next_req_id()
        args['id'] = req_id
        outstanding[req_id] = [Condition(), None]
        socketio.emit(name, args)

        def wait_for_response():
            cond: Condition = outstanding[req_id][0]
            with cond:
                cond.wait()
            return outstanding[req_id][1]

        loop = asyncio.get_running_loop()
        result = await loop.run_in_executor(None,  wait_for_response)
        del outstanding[req_id]
        return result


async def do_serve_grpc_async():
    server = grpc.aio.server()
    server.add_insecure_port('[::]:50051')
    add_DemoServicer_to_server(Servicer(), server)
    await server.start()
    await server.wait_for_termination()


def serve_grpc_async():
    asyncio.run(do_serve_grpc_async())


# def serve_grpc():
#     server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
#     add_DemoServicer_to_server(Servicer(), server)
#     server.add_insecure_port('[::]:50051')
#     server.start()
#     print("started grpc")
#     server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger("server")
    logger.info("starting gRPC")
    from threading import Thread
    t = Thread(target=serve_grpc_async, daemon=True)
    t.start()
    logger.info("starting socketio")
    socketio.run(app, debug=True, use_reloader=False)
