# Lightning Signer Web Demo

## Overview

This repository demonstrates the use of [Lightning Signer](https://gitlab.com/lightning-signer/docs/) compiled to Wasm and running in a browser.

The C-Lightning HSMD daemon is replaced with two proxies that transport the HSMD-wire protocol packets to the Wasm component.  The packets are first wrapped in gRPC, concentrated in a central proxy and then transmitted to the browser via Websockets.

**See the [Wasm Lightning Signer demo video](https://gitlab.com/lightning-signer/signer-web-demo/-/wikis/uploads/903ff5afe970b9e8a868487c89cfc8ec/wasm-signer-demo2s.webm)** - you will need a laptop/desktop screen to see the detail.

![System Diagram](doc/diagram1.svg)

## Main Components

* [C-Lightning customized](https://github.com/lightning-signer/c-lightning/tree/remote-hsmd) to work with Lightning Signer 
* The [Lightning Signer](https://gitlab.com/lightning-signer/rust-lightning-signer) crate compiled to Wasm
* `client.py` - replaces HSMD and proxies to server.py via gRPC
* `server.py` - listens to client.py on gRPC and proxies to the browser via Websocket.  Also serves HTML / Javascript UI and glue code.
* `app.js` - provides glue code for Websockets and keeps track of Signer handler instances
* [wasm-signer](wasm-signer) - an adapter using `wasm_bindgen` to talk to `app.js`. Invokes the Signer handler instance.
* A [HSMD-wire protocol handler](https://gitlab.com/lightning-signer/greenlight-signer) Rust crate

## Running

Clone the relevant repos:

```shell
git clone https://github.com/lightning-signer/c-lightning -b remote-hsmd
git clone https://gitlab.com/lightning-signer/rust-lightning-signer
git clone https://gitlab.com/lightning-signer/greenlight-signer
git clone https://gitlab.com/lightning-signer/signer-web-demo
```

In `./signer-web-demo`:
```shell
rustup target add wasm32-unknown-unknown
cargo install wasm-pack

# You might want to be in a Python virtualenv.

# The virtualenv must be shared with C-Lightning, because our client.py
# will be invoked by the C-Lightning integration test framework.

pip3 install -r requirements.txt

(cd wasm-signer/ &&  wasm-pack build --target web --dev)

./scripts/compile-protos

# Run the web / gRPC server
./server.py
```

Open a browser to `http://localhost:5000/`

In `./c-lightning` follow C-Lightning instructions to build, then:

```shell
ln -sf ../../signer-web-demo/client.py lightningd/remote_hsmd_web
export SUBDAEMON=hsmd:remote_hsmd_web
# Let client.py pretend it's the right version of hsmd
export GREENLIGHT_VERSION=`./lightningd/lightningd --version`
```

Finally, in `./c-lightning` run some integration tests and monitor in the browser:

```shell
# Run some integration tests and view the results in the browser.
# If using a Python virtualenv, see note above.
pytest -v --timeout=120 tests/test_pay.py
```

## Demo Notes

Demo policy failure "don't sign a revoked tx" with the following test: `tests/test_closing.py::test_penalty_htlc_tx_timeout`

## TODO

Look into unexpected failure:

```
FAILED tests/test_pay.py::test_setchannelfee_state - pyln.client.lightning.RpcError: RPC call failed: meth...
```
