#!/bin/env python3
import argparse
import array
import logging
import os
import struct
from io import FileIO, BufferedReader
from os import path
import socket
from threading import Thread
from typing import Tuple, Optional

import grpc

from service_pb2 import PingRequest, InitRequest, CallRequest, NewClientRequest
from service_pb2_grpc import DemoStub


THREADS = 10
COUNT = 10
logger: logging.Logger
instance: int = -1


def do_ping(server, i, j):
    result = server.Ping(PingRequest(body=f"req_{i}_{j}"))
    print(result.body)


def do_ping_loop(server, i):
    for j in range(COUNT):
        do_ping(server, i, j)


def load_test():
    with grpc.insecure_channel('localhost:50051') as channel:
        server = DemoStub(channel)
        threads = []
        for i in range(THREADS):
            t = Thread(target=lambda: do_ping_loop(server, i))
            t.start()
            threads.append(t)
        for t in threads:
            t.join()


def read_message(br: BufferedReader) -> Tuple[Optional[int], Optional[bytes]]:
    res = br.read(4)
    if res == b'':
        return None, None
    (length,) = struct.unpack(">L", res)
    msg = br.read(length)
    (message_type,) = struct.unpack(">H", msg[0:2])
    return message_type, msg


def write_typed_message(io: FileIO, message_type: int, body: bytes):
    assert message_type >= 0 and message_type < 65536
    msg = struct.pack(">H", message_type) + body
    write_message(io, msg)


def write_message(io, msg):
    io.write(struct.pack(">L", len(msg)))
    io.write(msg)


class Handler:
    def __init__(self, fd, server: Optional[DemoStub] = None, dbid=None, dummy=False):
        self.fd = fd
        self.io = FileIO(self.fd, mode="r+", closefd=False)
        self.br = BufferedReader(self.io)
        self.dbid = dbid
        self.dummy = dummy
        self.server = server

    def start(self):
        t = Thread(target=self.do_loop, daemon=True)
        t.start()

    def client_id(self):
        if self.dbid is None:
            return 0
        else:
            return self.fd

    def do_loop(self):
        while True:
            logger.info(f"@{instance}-{self.fd}-{self.dbid} read")
            try:
                message_type, msg = read_message(self.br)
                logger.info(f"@{instance}-{self.fd}-{self.dbid} message type {message_type}")
            except OSError as e:
                logger.warning(f"@{instance}-{self.fd}-{self.dbid} OS error {e}, ending")
                break
            if message_type is None:
                logger.info(f"@{instance}-{self.dbid} EOF, terminating loop for dbid {self.dbid}")
                break
            if message_type == 9:
                if self.dbid is not None:
                    raise "unexpected clientfd call on a per-channel handler"
                self.handle_clientfd(msg)
            elif self.server is not None:
                request = CallRequest(client_id=self.client_id(), instance_id=instance, body=msg)
                response = self.server.Call(request)
                logger.info(f"@{instance}-{self.fd}-{self.dbid} responding")
                write_message(self.io, response.body)

    def handle_clientfd(self, msg):
        logger.info(f"clientfd msg len {len(msg)} {msg}")
        offset = 2  # skip type
        peer_id = msg[offset:offset + 33]
        offset += 33
        (dbid, _caps) = struct.unpack(">QQ", msg[offset:])
        logger.info(f"@{instance} new fd for dbid {dbid} peer {peer_id}")
        (s1, s2) = socket.socketpair(socket.AF_UNIX)
        fd1 = s1.fileno()
        fd2 = s2.fileno()

        s1.detach()
        s2.detach()

        logger.info(f"new clientfd send={fd1} - listen={fd2}")
        fds = array.array('i', [fd1])
        if self.dummy:
            return
        write_typed_message(self.io, 109, b'')
        sock = socket.socket(fileno=self.fd)
        sock.sendmsg([b'\x01'], [(socket.SOL_SOCKET, socket.SCM_RIGHTS, fds)])
        sock.detach()
        request = NewClientRequest(instance_id=instance, client_id=fd2, dbid=dbid, peer_id=peer_id)
        server.NewClient(request)
        Handler(fd2, self.server, dbid).start()


if __name__ == '__main__':
    logpath = f"/tmp/signer_client_{os.getpid()}"
    logging.basicConfig(level=logging.INFO, filename=logpath)
    logger = logging.getLogger("client")

    parser = argparse.ArgumentParser(description='Remote hsmd grpc proxy (wire protocol wrapper method)')
    parser.add_argument('--test-fork', action='store_true', help='run a test')
    parser.add_argument('--version', action='store_true', help='fake version')

    # Ignored for compatibility with hsmd
    parser.add_argument('--dev-disconnect', metavar='N')
    parser.add_argument('--log-io', action='store_true')

    args = parser.parse_args()

    if args.version:
        print(os.environ.get("GREENLIGHT_VERSION"))
        exit()

    if args.test_fork:
        (s1, s2) = socket.socketpair(socket.AF_UNIX)
        br = BufferedReader(FileIO(s1.fileno(), "r", closefd=False))

        if os.fork() == 0:
            Handler(s2.fileno()).do_loop()
        else:
            stdin = BufferedReader(FileIO(0, "r"))
            while True:
                message_type, msg = read_message(stdin)
                if message_type is None:
                    break
                s1.send(struct.pack(">L", len(msg)))
                s1.send(msg)
                if message_type == 9:
                    resp_type, resp_msg = read_message(br)
                    logger.info(f"resp {resp_type}")
                    # read the sendmsg byte
                    br.read(1)

        exit()

    with grpc.insecure_channel('localhost:50051') as channel:
        server = DemoStub(channel)
        if path.exists("SIGNER_INSTANCE"):
            with open("SIGNER_INSTANCE", "r") as f:
                instance = int(f.read())
                logger.info(f"restart instance {instance}")
        else:
            with open("hsm_secret", "rb") as f:
                seed = f.read()
            instance = server.Init(InitRequest(seed=seed)).instance_id
            logger.info(f"instance {instance}")
            with open("SIGNER_INSTANCE", "w") as f:
                f.write(str(instance))

        Handler(3, server).do_loop()
