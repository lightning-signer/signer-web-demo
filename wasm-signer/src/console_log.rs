#[cfg(target_arch = "wasm32")] use alloc::format;

use log::{LevelFilter, Metadata, Record, Level};
#[cfg(target_arch = "wasm32")] use web_sys;

struct SimpleLogger;

static LOGGER: SimpleLogger = SimpleLogger;
#[allow(unused)]
const LOG_TRIM: usize = 80;

impl log::Log for SimpleLogger {
    fn enabled(&self, _metadata: &Metadata) -> bool {
        true
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            #[cfg(target_arch = "wasm32")] {
                let mut out = format!("{} - {}", record.level(), record.args());
                match record.level() {
                    Level::Error => web_sys::console::error_1(&out.into()),
                    Level::Warn => web_sys::console::warn_1(&out.into()),
                    _ => {
                        if LOG_TRIM > 0 {
                            out.truncate(LOG_TRIM);
                            out = out + " ...";
                        }
                        web_sys::console::info_1(&out.into())
                    },
                }
           }
        }
    }

    fn flush(&self) {}
}

pub(crate) fn setup_log() {
    log::set_logger(&LOGGER)
        .map(|()| log::set_max_level(LevelFilter::Info))
        .expect("create logger");
    debug!("Logging started");
}
