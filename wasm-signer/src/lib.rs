#![cfg_attr(all(not(feature = "std"), not(test)), no_std)]

extern crate alloc;
#[macro_use]
extern crate log;

use alloc::borrow::ToOwned;
use alloc::format;
use alloc::string::String;
use alloc::vec::Vec;
use core::convert::TryInto;
use core::str::FromStr;

use bitcoin::hashes::hex::ToHex;
use greenlight_protocol::msgs;
use greenlight_protocol::msgs::MemleakReply;
use greenlight_protocol::serde_bolt::to_vec;
use greenlight_signer::greenlight_protocol;
use greenlight_signer::greenlight_protocol::model::PubKey;
use greenlight_signer::greenlight_protocol::msgs::{Memleak, SerMessage, TypedMessage};
use greenlight_signer::handler::{ChannelHandler, Handler, RootHandler};
use lightning_signer::Arc;
use lightning_signer::bitcoin;
use lightning_signer::channel::ChannelSlot;
use lightning_signer::persist::{DummyPersister, Persist};
use log::LevelFilter;
use serde::Serialize;
use wasm_bindgen::prelude::*;
#[cfg(target_arch = "wasm32")] use web_sys;

use crate::console_log::setup_log;
use crate::utils::set_panic_hook;

mod console_log;
mod utils;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub struct ChannelProtocolHandler {
    handler: Arc<ChannelHandler>,
}

#[derive(Serialize)]
pub struct NodeStatus {
    pub node_id: String,
}


#[derive(Serialize)]
pub struct ChannelStatus {
    pub peer_id: String,
    pub dbid: u64,
    pub state: String,
    pub action: String,
    pub holder_commit_num: u64,
    pub counterparty_commit_num: u64,
}

#[wasm_bindgen]
pub struct CallResult {
    message: Vec<u8>,
    node_status: JsValue,
    channel_status: JsValue,
}

#[wasm_bindgen]
impl CallResult {
    #[wasm_bindgen]
    pub fn message(&self) -> Vec<u8> {
        self.message.clone()
    }

    #[wasm_bindgen]
    pub fn node_status(&self) -> JsValue {
        return self.node_status.clone()
    }

    #[wasm_bindgen]
    pub fn channel_status(&self) -> JsValue {
        return self.channel_status.clone()
    }
}

#[wasm_bindgen]
pub struct RootProtocolHandler {
    handler: Arc<RootHandler>,
}

#[wasm_bindgen]
impl RootProtocolHandler {
    #[wasm_bindgen]
    pub fn new(id: u64, seed_vec: Vec<u8>, allowlist_str: String) -> RootProtocolHandler {
        let persister: Arc<dyn Persist> = Arc::new(DummyPersister);
        let seed = seed_vec.as_slice().try_into().unwrap();
        let allowlist = allowlist_str.split("\n")
            .map(str::to_owned)
            .filter(|s| !s.starts_with("#") && !s.is_empty())
            .collect();
        let handler = RootHandler::new(id, Some(seed), persister, allowlist);
        // TODO remove in production :)
        debug!("SEED {}", seed.to_hex());
        RootProtocolHandler {
            handler: Arc::new(handler)
        }
    }

    #[wasm_bindgen(js_name = toString)]
    pub fn to_string(&self) -> String {
        format!("RootProtocolHandler({})", self.handler.client_id())
    }

    #[wasm_bindgen]
    pub fn handle(&self, mut ser_msg: Vec<u8>) -> CallResult {
        let len = ser_msg.len();
        let msg = msgs::read_unframed(&mut ser_msg, len as u32)
            .expect("deserialize");
        info!("handle {:?}", msg);
        let result = self.handler.handle(msg);
        let node_status = NodeStatus {
            node_id: self.handler.node.get_id().to_hex(),
        };
        match result {
            Ok(reply) => {
                let message = reply.vec_serialize();
                CallResult {
                    message,
                    node_status: JsValue::from_serde(&node_status).unwrap(),
                    channel_status: JsValue::NULL
                }
            }
            Err(e) => {
                error!("{:?}", e);
                // TODO provide a real client error reply
                let reply = MemleakReply { result: false };
                let message = reply.vec_serialize();
                CallResult {
                    message,
                    node_status: JsValue::from_serde(&node_status).unwrap(),
                    channel_status: JsValue::NULL
                }
            }
        }
    }

    #[wasm_bindgen]
    pub fn new_dbid(&self, peer_id_vec: Vec<u8>, dbid: u64) -> ChannelProtocolHandler {
        let peer_id = PubKey(peer_id_vec.as_slice().try_into().unwrap());
        let handler = self.handler.for_new_client(peer_id, dbid);
        ChannelProtocolHandler {
            handler: Arc::new(handler)
        }
    }
}

#[wasm_bindgen]
impl ChannelProtocolHandler {
    #[wasm_bindgen(js_name = toString)]
    pub fn to_string(&self) -> String {
        format!("ChannelProtocolHandler({})", self.handler.client_id())
    }

    #[wasm_bindgen]
    pub fn handle(&self, mut ser_msg: Vec<u8>) -> CallResult {
        let len = ser_msg.len();
        let msg = msgs::read_unframed(&mut ser_msg, len as u32)
            .expect("deserialize");
        let debug_msg = format!("{:?}", msg);
        info!("{}", debug_msg);
        let result = self.handler.handle(msg);
        let node_status = NodeStatus {
            node_id: self.handler.node.get_id().to_hex(),
        };
        let channel_mutex = self.handler.node.get_channel(&self.handler.channel_id).expect("channel");
        let channel = channel_mutex.lock().unwrap();
        let mut channel_status = match &*channel {
            ChannelSlot::Stub(_) => {
                ChannelStatus {
                    peer_id: self.handler.peer_id.to_hex(),
                    dbid: self.handler.dbid,
                    state: "new".to_owned(),
                    action: "".to_owned(),
                    holder_commit_num: 0,
                    counterparty_commit_num: 0,
                }
            }
            ChannelSlot::Ready(c) => {
                let enforcement_state = &c.enforcement_state;
                let state = if enforcement_state.mutual_close_signed {
                    "closed"
                } else {
                    "ready"
                };
                let action = debug_msg.split("(").next().unwrap_or("").to_owned();
                ChannelStatus {
                    peer_id: self.handler.peer_id.to_hex(),
                    dbid: self.handler.dbid,
                    state: state.to_owned(),
                    action,
                    holder_commit_num: enforcement_state.next_holder_commit_num,
                    counterparty_commit_num: enforcement_state.next_counterparty_commit_num,
                }
            }
        };

        match result {
            Ok(reply) => {
                let message = reply.vec_serialize();
                CallResult {
                    message,
                    node_status: JsValue::from_serde(&node_status).unwrap(),
                    channel_status: JsValue::from_serde(&channel_status).unwrap(),
                }
            }
            Err(e) => {
                let error_msg = format!("{:?}", e);
                error!("{}", error_msg);
                channel_status.state = "broken".to_owned();
                channel_status.action = error_msg;
                // TODO provide a real client error reply
                let reply = MemleakReply { result: false };
                let message = reply.vec_serialize();
                CallResult {
                    message,
                    node_status: JsValue::from_serde(&node_status).unwrap(),
                    channel_status: JsValue::from_serde(&channel_status).unwrap()
                }
            }
        }
    }
}


#[wasm_bindgen]
pub struct Tester {
}

#[wasm_bindgen]
impl Tester {
    #[wasm_bindgen]
    pub fn new() -> Self {
        Tester {}
    }

    #[wasm_bindgen]
    pub fn memleak(&self) -> Vec<u8> {
        let msg = Memleak {};
        let mut value_buf = to_vec(&msg).expect("serialize");
        let mut buf = Memleak::TYPE.to_be_bytes().to_vec();
        buf.append(&mut value_buf);
        buf
    }
}

#[wasm_bindgen]
pub fn setup() {
    set_panic_hook();
    setup_log();
}

#[allow(unused)]
#[cfg(target_arch = "wasm32")]
fn randomize_buffer(seed: &mut [u8; 32]) {
    use web_sys::{window, Crypto};

    let window = window().expect("window");
    let crypto: Crypto = window.crypto().expect("crypto");
    crypto
        .get_random_values_with_u8_array(seed)
        .expect("random");
}

#[wasm_bindgen]
pub fn set_log_level(level: String) {
    log::set_max_level(LevelFilter::from_str(&level).expect("level name"));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn tester_memleak() {
        let tester = Tester::new();
        let msg = tester.memleak();
        println!("{:?}", msg)
    }
}
