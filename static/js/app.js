import init, {
    set_log_level,
    setup,
    RootProtocolHandler,
    Tester,
} from '../pkg/wasm_signer.js';

function buf2hex(buffer) { // buffer is an ArrayBuffer
    return [...new Uint8Array(buffer)]
        .map(x => x.toString(16).padStart(2, '0'))
        .join('');
}

let root_handlers = {};
let client_handlers = {};
let allowlist = [];

const socket = io('http://localhost:5000')

socket.on('connect', () => {
    console.log("socket connected");
});

socket.on('init', (req) => {
    let instanceId = req.instance_id;
    let seed = new Uint8Array(req.seed);
    console.log(`init ${req.id} inst=${instanceId} seed=${buf2hex(req.seed)}`);
    if (instanceId in root_handlers) {
        console.log(`ignore init on ${instanceId} because it already exists`);
    } else {
        root_handlers[instanceId] = RootProtocolHandler.new(BigInt(instanceId), seed, allowlist)
        client_handlers[instanceId] = {}
    }
    socket.emit('response', {id: req.id});
});


socket.on('root_request', (req) => {
    let instanceId = req.instance_id;
    let body = new Uint8Array(req.body);
    // console.log(`root_request ${req.id} inst=${instanceId} ${buf2hex(body)}`);
    if (!(instanceId in root_handlers)) {
        throw "not inited"
    }
    let result = root_handlers[instanceId].handle(body)
    socket.emit('response', {id: req.id, body: result.message()});
});

socket.on('new_client', (req) => {
    let instanceId = req.instance_id;
    let peerId = new Uint8Array(req.peer_id);
    let client_id = BigInt(req.client_id);
    let dbid = req.dbid;
    console.log(`new_client ${req.id} inst=${instanceId} client=${client_id} dbid=${dbid}`);
    if (!(instanceId in root_handlers))
        throw "missing root handler";
    if (client_id in client_handlers[instanceId]) {
        console.log(`already have client ${client_id}, overwrite`);
    }
    if (dbid === 0)
        client_handlers[instanceId][client_id] = root_handlers[instanceId]
    else
        client_handlers[instanceId][client_id] = root_handlers[instanceId].new_dbid(peerId, BigInt(dbid));
    socket.emit('response', {id: req.id});
});

function trim_id(id) {
    return id.substring(0, 10);
}

function insert_channel_status(instance_id, node_status, channel_status) {
    let dbid = channel_status.dbid;
    if ($(`#inst${instance_id}`).length === 0) {
        $(`<tr id='inst${instance_id}'>
<td>${instance_id}</td>
<td>${trim_id(node_status.node_id)}</td>
<td>
<table class="client table-bordered">
<tr id="header_${instance_id}">
<th>DB ID</th>
<th>Peer ID</th>
<th>Status</th>
<th>Local/Remote Commit</th>
<th>Action</th>
</tr>
</table>
</td>
</tr>`)
            .insertAfter("#header");
    }

    let el_id = `#client_${instance_id}_${dbid}`;

    if ($(el_id).length === 0)
        $(`<tr id='client_${instance_id}_${dbid}'></tr>`).insertAfter(`#header_${instance_id}`)

    $(el_id).empty().append(
        `
<td>${dbid}</td>
<td>${trim_id(channel_status.peer_id)}</td>
<td class="state ${channel_status.state}">${channel_status.state}</td>
<td>${channel_status.holder_commit_num} / ${channel_status.counterparty_commit_num}</td>
<td class="action">${channel_status.action}</td>
`);
}

socket.on('client_request', (req) => {
    let instanceId = req.instance_id;
    let client_id = BigInt(req.client_id);
    let body = new Uint8Array(req.body);
    if (!(instanceId in client_handlers)) {
        throw "missing root handler";
    }
    if (!(client_id in client_handlers[instanceId])) {
        throw "missing client";
    }
    let result = client_handlers[instanceId][client_id].handle(body)
    if (result.channel_status())
        insert_channel_status(instanceId, result.node_status(), result.channel_status());

    socket.emit('response', {id: req.id, body: result.message()});
});

function insert_test() {
    let node_status = {};
    node_status.node_id = "yyy";
    let channel_status = {};
    channel_status.peer_id = "zzz";
    channel_status.dbid = 20;
    channel_status.state = "good";
    channel_status.holder_commit_num = 5;
    channel_status.counterparty_commit_num = 6;
    insert_channel_status(10, node_status, channel_status);
}

async function run() {
    // Load the C-Lightning integration test allowlist
    const response = await fetch('/static/allowlist.txt');
    allowlist = await response.text();

    // Load the wasm
    await init();
    setup();
    set_log_level("INFO");

    // insert_test();
    // let tester = Tester.new();

    console.log('ready');
}

run();
